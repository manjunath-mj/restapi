const express=require('express');

const bodyParser = require('body-parser'); 

const {mongoos}=require('./db.js');
var app=express();
var cors = require('cors');
app.use(cors());

// middleware
// var bgm =(req, res,next)=>{
//   console.log('Bgm')
//   next();
// }
// var mumbai =(req, res,next)=>{
//   console.log('mumbai')
//   next();
// }
// var goa =(req, res,next)=>{
//   console.log('Goa')
//   next();
// }


// app.use('/middleware' , bgm, mumbai, goa,bgm,mumbai);

var employeeController=require('./controllers/employeeController.js');

app.use(bodyParser.json());
// var port = 3100;

var port=process.env.PORT||3010;

app.listen(port, (req, res) => {
 console.log(`port --> ${port}`);
  // console.log("portt --> 3000" );
});

app.use('/_emp',employeeController); //page name
