const mongoose = require('mongoose');

var Employee = mongoose.model('Employee',
{
       name: { type: String,trim:true },
       position: { type: String },
       office: { type: String },
       salary: { type: Number, min:5  }
      // ,department:{type:String}
});

// // or
//
// // var Employee=mongoose.model('Employee',{
// //     name:{type:string},
// //     postion:{type:string},
// //     office:{type:string},
// //     salary:{type:number},
// // },'emp_tableName'); //collection
// module.exports={
//   // Employee:Employee;
//      Employee //or
// };

module.exports = { Employee };


// var bys = new mongoose.Schema({
//       name:{type:String, trim:true},
//       doj:{type:Date, trim:true},
//       dob:{type:Date, trim:true},
//       salary:{type:Number, trim:true},
//       email:{type:String, trim:true},
//       worklocation:{type:String, trim:true},
//       // mobile:{type:Number, min:10, max:10, required: true},
//       mobile:{type:Number, trim:true},
//       paddress:{type:String, trim:true}
//   });
//   mongoose.model('emp',bys,'bysMean');