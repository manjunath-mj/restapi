const express=require('express');

var router=express.Router();

var ObjectId=require('mongoose').Types.ObjectId; //

var {Employee}=require('../models/employee');

//select
router.get('/', (req,res)=>{
  Employee.find( (err,docs)=>{ //find is function
    
    if(!err){
      res.send(docs);
      console.log(docs)
      // res.send("<h1>  Hello</h1>");
    }
    else{
      console.log('eerror in retriving employee : '+JSON.stringify(err, undefine,2));
    }
  });
});


//go to postman url ==> localhost:3000/_emp/_id 
router.get('/:a',(req, res)=>{

  if(!ObjectId.isValid(req.params.a))
    return res.status(400)
              .send(`Manju--> No received with given Id: ${req.params.a} `);   
    Employee.findById(req.params.a, (err,doc,undefined)=>{
      if(!err){
        res.send(doc);
      }else{
        console.log('Error in retriving Employee' +JSON.stringify(err, undefined, 2));
      }
    });
});

//insert
router.post('/',(req,res)=>{
  var emp = new Employee({
    name:req.body.name,
    position:req.body.position,
    office:req.body.office,
    salary:req.body.salary,
    department:req.body.department,
  });
  // res.send("<h1>Hllo </h1>");
  emp.save((err,docs,undefine)=>{
    if(!err){
      res.send(docs);
    }
    else{
      console.log('error in inserting  : '+JSON.stringify(err, undefine,2));
    }
  });
});


//update
router.put('/:_id',(req,res)=>{
  if(!ObjectId.isValid(req.params._id))
    return res.status(400).send(`Manju No record with given id : ${req.params._id}`);
    var emp={
      name:req.body.name,
      position:req.body.position,
      office:req.body.office,
      salary:req.body.salary,
    };
    Employee.findByIdAndUpdate(req.params._id,{$set:emp},{new:true}, (err,doc,undefined)=>{
      if(!err){ res.send(doc);}
      else{
        console.log('Error in Employee update : ' +JSON.stringify(err,undefined,2));
      }
    });
});

//delete
router.delete('/:_id',(req,res)=>{
    if(!ObjectId.isValid(req.params._id))
      return res.status(400).send(`No record with Given ID to Delete : ${req.params._id}`);

      Employee.findByIdAndRemove(req.params._id,(err, doc, undefined)=>{
        if(!err) {
          res.send(doc);
          console.log(doc);
        }else{
          console.log('Error in Employee Delete :' +JSON.stringify(err,undefined,2));
        }
      });
});

module.exports = router;